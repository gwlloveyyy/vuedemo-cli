/*!
 * ZUI: 排序 - v1.9.2 - 2020-07-09
 * http://openzui.com
 * GitHub: https://github.com/easysoft/zui.git
 * Copyright (c) 2020 cnezsoft.com; Licensed MIT
 */

/* ========================================================================
 * ZUI: sortable.js
 * http://openzui.com
 * ========================================================================
 * Copyright (c) 2014-2016 cnezsoft.com; Licensed MIT
 * ======================================================================== */

+(function ($, window, document) {
  'use strict'

  if (!$.fn.droppable) {
    console.error('Sortable requires droppable.js')
    return
  }

  const NAME = 'zui.sortable'
  const DEFAULTS = {
    selector: 'li,div',
    dragCssClass: 'invisible',
    sortingClass: 'sortable-sorting'
  }
  const STR_ORDER = 'order'

  const Sortable = function (element, options) {
    const that = this
    that.$ = $(element)
    that.options = $.extend({}, DEFAULTS, that.$.data(), options)
    that.init()
  }

  Sortable.DEFAULTS = DEFAULTS
  Sortable.NAME = NAME

  Sortable.prototype.init = function () {
    const that = this
    const $root = that.$
    const options = that.options
    const selector = options.selector
    const containerSelector = options.containerSelector
    const sortingClass = options.sortingClass
    const dragCssClass = options.dragCssClass
    const targetSelector = options.targetSelector
    const isReverse = options.reverse
    let orderChanged

    const markOrders = function ($items) {
      $items = $items || that.getItems(1)
      const itemsCount = $items.length
      if (itemsCount) {
        $items.each(function (itemIndex) {
          const itemOrder = isReverse ? itemsCount - itemIndex : itemIndex
          $(this).attr('data-' + STR_ORDER, itemOrder).data(STR_ORDER, itemOrder)
        })
      }
    }

    markOrders()

    $root.droppable({
      handle: options.trigger,
      target: targetSelector || (containerSelector ? (selector + ',' + containerSelector) : selector),
      selector: selector,
      container: $root,
      always: options.always,
      flex: true,
      lazy: options.lazy,
      canMoveHere: options.canMoveHere,
      dropToClass: options.dropToClass,
      before: options.before,
      nested: !!containerSelector,
      mouseButton: options.mouseButton,
      stopPropagation: options.stopPropagation,
      start: function (e) {
        if (dragCssClass) e.element.addClass(dragCssClass)
        orderChanged = false
        that.trigger('start', e)
      },
      drag: function (e) {
        $root.addClass(sortingClass)
        if (e.isIn) {
          const $ele = e.element
          const $target = e.target
          const isContainer = containerSelector && $target.is(containerSelector)

          if (isContainer) {
            if (!$target.children(selector).filter('.dragging').length) {
              $target.append($ele)
              var $items = that.getItems(1)
              markOrders($items)
              that.trigger(STR_ORDER, {
                list: $items,
                element: $ele
              })
            }
            return
          }

          const eleOrder = $ele.data(STR_ORDER)
          const targetOrder = $target.data(STR_ORDER)
          if (eleOrder === targetOrder) return markOrders($items)
          else if (eleOrder > targetOrder) {
            $target[isReverse ? 'after' : 'before']($ele)
          } else {
            $target[isReverse ? 'before' : 'after']($ele)
          }
          orderChanged = true
          var $items = that.getItems(1)
          markOrders($items)
          that.trigger(STR_ORDER, {
            list: $items,
            element: $ele
          })
        }
      },
      finish: function (e) {
        if (dragCssClass && e.element) e.element.removeClass(dragCssClass)
        $root.removeClass(sortingClass)
        that.trigger('finish', {
          list: that.getItems(),
          element: e.element,
          changed: orderChanged
        })
      }
    })
  }

  Sortable.prototype.destroy = function () {
    this.$.droppable('destroy')
    this.$.data(NAME, null)
  }

  Sortable.prototype.reset = function () {
    this.destroy()
    this.init()
  }

  Sortable.prototype.getItems = function (onlyElements) {
    const $items = this.$.find(this.options.selector).not('.drag-shadow')
    if (!onlyElements) {
      return $items.map(function () {
        const $item = $(this)
        return {
          item: $item,
          order: $item.data('order')
        }
      })
    }
    return $items
  }

  Sortable.prototype.trigger = function (name, params) {
    return $.zui.callEvent(this.options[name], params, this)
  }

  $.fn.sortable = function (option) {
    return this.each(function () {
      const $this = $(this)
      let data = $this.data(NAME)
      const options = typeof option == 'object' && option

      if (!data) $this.data(NAME, (data = new Sortable(this, options)))
      else if (typeof option == 'object') data.reset()

      if (typeof option == 'string') data[option]()
    })
  }

  $.fn.sortable.Constructor = Sortable
}(jQuery, window, document))

import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import cookie from './plugins/cookie'

import './styles/reset.css'

import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(Element)

import Moment from 'moment'
Vue.prototype.$moment = Moment

Vue.config.productionTip = false

new Vue({
  router,
  store,
  cookie,
  render: h => h(App)
}).$mount('#app')

import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  // 路由模式
  mode: 'history',
  // 路由集合
  routes: [
    {
      path: '/',
      name: 'Layout',
      redirect: '/Home',
      component: () => import('@/Layout/index.vue'),
      children: [
        {
          path: '/home',
          name: 'Home',
          component: () => import(/* webpackChunkName: "Home" */ '../views/Home.vue')
        },
        {
          path: '/form',
          name: 'Form',
          props: (route) => ({ 'route': route }),
          component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
        }
        // ...
      ]
    }
  ]
})

import request from '@/utils/request'

// 测试接口
export function queryBudgetOrder (query) {
  return request({
    url: '/users',
    method: 'get',
    params: query
  })
}
